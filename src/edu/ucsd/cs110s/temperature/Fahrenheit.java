/**
 * 
 */
package edu.ucsd.cs110s.temperature;

/**
 * @author cs110sam
 *
 */
public class Fahrenheit extends Temperature
{
public Fahrenheit(float t)
{
super(t);
}
public String toString()
{
// TODO: Complete this method
return getValue() + " F";
}
@Override
public Temperature toCelsius() {
	return new Celsius( 5 * (getValue() - 32)/9 );
}
@Override
public Temperature toFahrenheit() {
	return this;
}
}
