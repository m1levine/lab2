/**
 * 
 */
package edu.ucsd.cs110s.temperature;

/**
 * @author cs110sam
 *
 */
public class Celsius extends Temperature
{
public Celsius(float t)
{
super(t);
}
public String toString()
{
//string for temperature
return (getValue())
 + " C";
}
@Override
public Temperature toCelsius() {
	return this;
}
@Override
public Temperature toFahrenheit() {
	// convert to fahrenheit
	float value = getValue();
	value = ((value * 9)/5) + 32;
	return new Fahrenheit (value);
}
}
